package ru.t1.rleonov.tm.component;

import ru.t1.rleonov.tm.api.repository.ICommandRepository;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.command.project.*;
import ru.t1.rleonov.tm.command.system.*;
import ru.t1.rleonov.tm.command.task.*;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rleonov.tm.exception.system.CommandNotSupportedException;
import ru.t1.rleonov.tm.model.Project;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.repository.CommandRepository;
import ru.t1.rleonov.tm.repository.ProjectRepository;
import ru.t1.rleonov.tm.repository.TaskRepository;
import ru.t1.rleonov.tm.service.*;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    private void initDemoData() {
        projectService.add(new Project("FIRST PROJECT", Status.COMPLETED));
        projectService.add(new Project("A SECOND PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("THIRD PROJECT", Status.NOT_STARTED));

        taskService.add(new Task("FIRST TASK", Status.COMPLETED));
        taskService.add(new Task("A SECOND TASK", Status.IN_PROGRESS));
        taskService.add(new Task("THIRD TASK", Status.NOT_STARTED));
    }

    private void initLogger() {
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***")));
    }

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndex());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String command) {
        if (command == null) throw new CommandNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArguments(final String[] args) {
        final String arg = args[0];
        try {
            processArgument(arg);
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    private void processArgument(final String arg) {
        if (arg == null) throw new ArgumentNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void runApplication(final String[] args) {
        initDemoData();
        initLogger();
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
