package ru.t1.rleonov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public final static String COMMAND = "task-clear";

    public final static String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
