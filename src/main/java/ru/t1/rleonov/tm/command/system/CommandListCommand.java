package ru.t1.rleonov.tm.command.system;

import ru.t1.rleonov.tm.api.model.ICommand;
import ru.t1.rleonov.tm.command.AbstractCommand;
import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show application commands.";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
