package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public final static String COMMAND = "task-complete-by-index";

    public final static String DESCRIPTION = "Complete task by index.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
