package ru.t1.rleonov.tm.command.project;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.util.TerminalUtil;
import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public final static String COMMAND = "project-change-status-by-id";

    public final static String DESCRIPTION = "Change project status by id.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusById(id, status);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
